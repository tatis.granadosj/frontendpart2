import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {ReportComponent} from "../../dashboard/report/report.component";

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<ReportComponent> {
  canDeactivate(
    component: ReportComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot):  boolean {
    if(component.sendMessage()==false){
      alert('Do not save changed, Are you sure exit form?');
    }
    console.log('running canDeactivate');
    return true;
  }

}
