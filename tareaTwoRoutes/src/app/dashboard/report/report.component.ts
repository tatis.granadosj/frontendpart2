import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  message: string = '';
  send = false;
  constructor() { }

  ngOnInit(): void {
  }

  sendMessage() {
    return this.message.length === 0;
  }



}
