import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SalesComponent} from "./sales/sales.component";
import {InventoryComponent} from "./inventory/inventory.component";
import {ReportComponent} from "../dashboard/report/report.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {path: 'inventory', component: InventoryComponent},
      {path: 'reports', component: ReportComponent},
      {path: 'sales', component: SalesComponent},
      {path: '**', redirectTo: 'sales'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {
}
