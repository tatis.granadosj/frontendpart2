import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from "./administration/users/users.component";
import {ReportComponent} from "./dashboard/report/report.component";
import {Error404Component} from "./errors/error404/error404.component";
import {RolesComponent} from "./administration/roles/roles.component";
import {AdministrationComponent} from "./administration/administration/administration.component";
import {HomeComponent} from "./home/home/home.component";
import {CanDeactivateGuard} from "./shared/guards/can-deactivate.guard";
import {CanActivateGuard} from "./shared/guards/can-activate.guard";

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'administration',component: AdministrationComponent,
    canActivate: [CanActivateGuard],
    canActivateChild: [CanActivateGuard],
    children: [
      {path:'permissions', component:ReportComponent},
      {path:'roles', component:RolesComponent},
      {path:'users', component:UsersComponent}
    ]
  },
  {
    path: 'dashboard', component: ReportComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
  },
  {
    path: '**', component: Error404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
