import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  getUsers(): Observable<User[]> {
    return of([
      {
        id: '1',
        firstName: 'Rommel',
        lastName: 'Loayza',
        dateOfBirth: '2001-03-08'
      },
      {
        id: '2',
        firstName: 'Surimana',
        lastName: 'Ponce',
        dateOfBirth: '1987-04-07'
      },
      {
        id: '3',
        firstName: 'Lizbeth',
        lastName: 'Granados',
        dateOfBirth: '1995-09-03'
      }
    ]);
  }
}
