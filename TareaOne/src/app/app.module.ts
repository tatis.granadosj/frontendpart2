import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import {SharedModule} from "./shared/shared.module";
import {AdministrationModule} from "./administration/administration.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {IntegrationModule} from "./integration/integration.module";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        RouterModule,
        AdministrationModule,
        DashboardModule,
        IntegrationModule,
        SharedModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
