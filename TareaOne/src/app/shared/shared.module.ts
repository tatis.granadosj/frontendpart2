import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IsMyBirthdayPipe} from './pipes/is-my-birthday.pipe';


@NgModule({
  declarations: [
    IsMyBirthdayPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IsMyBirthdayPipe
  ]
})
export class SharedModule {
}
