import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isMyBirthday'
})
export class IsMyBirthdayPipe implements PipeTransform {

  transform(value: string) {
    const now = new Date();
    const nowDay= now.getDate();
    const nowMonth= now.getMonth()+1;

    const valueDate = new Date(value)
    const valueDay = valueDate.getDate()+1;
    const valueMonth = valueDate.getMonth()+1;

    // return + nowDay +'/'+ nowMonth  +'---'+ valueDay +'/'+valueMonth;

    if (nowDay === valueDay && nowMonth === valueMonth){
      return "HAPPY BIRTHDAY !";
    }else {
      return value;
    }
  }
}
