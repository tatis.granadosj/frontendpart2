import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report/report.component';
import {UiComponentsModule} from "../ui-components/ui-components.module";



@NgModule({
  declarations: [
    ReportComponent
  ],
  imports: [
    CommonModule,
    UiComponentsModule
  ]
})
export class DashboardModule { }
