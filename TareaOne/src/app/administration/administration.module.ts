import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import {UiComponentsModule} from "../ui-components/ui-components.module";
import {IntegrationModule} from "../integration/integration.module";
import {SharedModule} from "../shared/shared.module";


@NgModule({
    declarations: [
        UsersComponent
    ],
    exports: [
        UsersComponent
    ],
    imports: [
        CommonModule,
        UiComponentsModule,
        IntegrationModule,
        SharedModule
    ]
})
export class AdministrationModule { }
