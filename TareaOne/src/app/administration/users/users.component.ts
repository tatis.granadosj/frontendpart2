import { Component, OnInit } from '@angular/core';
import {UserService} from "../../integration/services/user.service";
import {User} from "../../integration/models/user";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  usersData: User[] = []

  constructor(private readonly usersService:UserService) { }

  ngOnInit(): void {
    this.usersService.getUsers().subscribe((users:User[])=>{
      this.usersData = users;
    }).unsubscribe();
  }

}
