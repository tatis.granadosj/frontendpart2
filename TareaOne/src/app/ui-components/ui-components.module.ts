import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { ChartComponent } from './chart/chart.component';
import { UserFormComponent } from './user-form/user-form.component';



@NgModule({
  declarations: [
    TableComponent,
    ChartComponent,
    UserFormComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    TableComponent,
    ChartComponent,
    UserFormComponent
  ]
})
export class UiComponentsModule { }
