import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Basetool} from "../basetool";

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit, Basetool {

  @Output()
  eventEmitter: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  userId: string = '';

  constructor() { }

  emitValue(): void {
    this.eventEmitter.emit('calculating...');
  }

  ngOnInit(): void {
  }

}
