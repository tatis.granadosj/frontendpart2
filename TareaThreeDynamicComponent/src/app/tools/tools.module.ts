import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolsRoutingModule } from './tools-routing.module';
import { MyToolsComponent } from './my-tools/my-tools.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { EditorComponent } from './editor/editor.component';
import { NotepadComponent } from './notepad/notepad.component';


@NgModule({
  declarations: [
    MyToolsComponent,
    ToolBarComponent,
    CalculatorComponent,
    EditorComponent,
    NotepadComponent
  ],
  imports: [
    CommonModule,
    ToolsRoutingModule
  ]
})
export class ToolsModule { }
