import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MyToolsComponent} from "./my-tools/my-tools.component";

const routes: Routes = [
  {
    path: 'myTools', component: MyToolsComponent
  },
  {
    path: '**', redirectTo: 'tools'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolsRoutingModule { }
