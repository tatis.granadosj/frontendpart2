import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Basetool} from "../basetool";

@Component({
  selector: 'app-notepad',
  templateUrl: './notepad.component.html',
  styleUrls: ['./notepad.component.css']
})
export class NotepadComponent implements OnInit,  Basetool {

  @Output()
  eventEmitter: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  userId: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  emitValue(): void {
    this.eventEmitter.emit('notepad...');
  }

}
