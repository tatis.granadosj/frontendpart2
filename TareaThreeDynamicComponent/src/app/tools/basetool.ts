import {EventEmitter} from "@angular/core";

export interface Basetool {
  userId: string;
  eventEmitter: EventEmitter<any>;
}
