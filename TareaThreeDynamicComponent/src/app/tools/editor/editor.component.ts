import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Basetool} from "../basetool";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit,  Basetool {

  @Output()
  eventEmitter: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  userId: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  emitValue(): void {
    this.eventEmitter.emit('editor...');
  }

}
