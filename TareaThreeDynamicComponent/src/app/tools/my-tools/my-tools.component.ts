import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver, OnDestroy,
  OnInit,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Basetool} from "../basetool";
import {ActivatedRoute} from "@angular/router";
import {CalculatorComponent} from "../calculator/calculator.component";
import {EditorComponent} from "../editor/editor.component";
import {NotepadComponent} from "../notepad/notepad.component";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-my-tools',
  templateUrl: './my-tools.component.html',
  styleUrls: ['./my-tools.component.css']
})
export class MyToolsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('toolContainer', {read: ViewContainerRef})

  private container!: ViewContainerRef;
  private store: Map<string, Type<Basetool>> = new Map<string, Type<Basetool>>();

  private subscription: Subscription = new Subscription();

  constructor(private readonly componentFactoryResolver: ComponentFactoryResolver, private readonly activatedRoute: ActivatedRoute) {
    this.store.set('calculator', CalculatorComponent);
    this.store.set('editor', EditorComponent);
    this.store.set('notepad', NotepadComponent);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const componentName = this.activatedRoute.snapshot.paramMap.get('type');
    if (!componentName) {
      return;
    }

    const componentType = this.store.get(componentName);
    if (!componentType) {
      return;
    }

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory<Basetool>(componentType);
    // this.container.createComponent(componentFactory);
    let reference = this.container.createComponent(componentFactory);
    reference.instance.userId = '123';
    reference.instance.eventEmitter.subscribe(
      next =>alert(next)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
