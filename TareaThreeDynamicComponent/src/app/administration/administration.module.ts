import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { AdministrationComponent } from './administration/administration.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    UsersComponent,
    RolesComponent,
    PermissionsComponent,
    AdministrationComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class AdministrationModule { }
