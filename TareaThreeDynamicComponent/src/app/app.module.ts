import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AdministrationModule} from "./administration/administration.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {ProductsRoutingModule} from "./products/products-routing.module";
import {ErrorsModule} from "./errors/errors.module";
import {ToolsModule} from "./tools/tools.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdministrationModule,
    DashboardModule,
    ProductsRoutingModule,
    ErrorsModule,
    ToolsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
