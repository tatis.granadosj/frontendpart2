import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lib-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.css']
})
export class RadioButtonComponent implements OnInit {
  @Output() output = new EventEmitter<any>();
  public option = 'option';

  constructor() {
  }

  ngOnInit(): void {
  }

  onBlur(data: string): void {
    this.output.emit(data);
  }

  onClick(): void {
    this.output.emit('click radio button');
  }

}
