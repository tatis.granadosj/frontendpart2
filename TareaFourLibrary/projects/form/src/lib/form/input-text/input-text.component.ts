import { EventEmitter } from '@angular/core';
import {Component, Input, OnInit, Output} from '@angular/core';
import {FormItem} from "../../api/form-item";

@Component({
  selector: 'lib-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.css']
})
export class InputTextComponent implements OnInit {

  @Output() output = new EventEmitter<string>();
  public value = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  onBlur(data: string): void {
    this.output.emit(data);
  }

  onClick(): void {
    this.output.emit('click input');
  }
}
