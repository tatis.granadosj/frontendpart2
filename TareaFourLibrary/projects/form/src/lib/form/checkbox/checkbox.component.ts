import { EventEmitter } from '@angular/core';
import {Component, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lib-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Output() output = new EventEmitter<string>();
  public value = '';

  constructor() { }

  ngOnInit(): void {
  }
  onBlur(data: string): void {
    this.output.emit(data);
  }

  onClick(): void {
    this.output.emit('onClick event UP in input text component');
  }
}
