import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './input-text/input-text.component';
import { SelectFieldComponent } from './select-field/select-field.component';
import { RadioButtonComponent } from './radio-button/radio-button.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    InputTextComponent,
    SelectFieldComponent,
    RadioButtonComponent,
    CheckboxComponent
  ],
    imports: [
        CommonModule,
        FormsModule
    ],
  exports: [
    CheckboxComponent,
    InputTextComponent,
    RadioButtonComponent,
    SelectFieldComponent
  ]
})
export class FormModule { }
